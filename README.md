# Installation

## Install node dependencies
```
npm install
```

## Prepare ads data

```
cp ads.json.example ads.json
```

Feel free to edit ```ads.json``` as you wish.

Note: Please see **Get random ads API** section to see how data format is

# Run server

```
node index.js
```

And then you can access from http://localhost:3000/ads

## Difference port

```
PORT=8888 node index.js
```

Then you can access from http://localhost:8888/ads

# API

## Get random ads

```GET /ads```

### Image Response
```json
{
    "id": "image_1",
    "type": "image",
    "url": "https://example.com/ads-image1.jpg",
    "width": 500,
    "height": 500,
    "duration": 5,
    "enabled": true
}
```

| Key | Type | Description |
|-|-|-|
| id | String | Unique ID |
| type | String | "image" |
| url | String | Image URL |
| width | Integer | Image's width in pixels |
| height | Integer | Image's height in pixels |
| duration | Integer | Skipable duration in seconds |
| enabled | Boolean | Enable or disable |

### Video Response

```json
{
    "id": "video_1",
    "type": "video",
    "url": "https://example.com/ads-video1.mp4",
    "width": 640,
    "height": 480,
    "video_length": 30,
    "duration": 5,
    "enabled": true
}
```

| Key | Type | Description |
|-|-|-|
| id | String | Unique ID |
| type | String | "video" |
| url | String | Video URL |
| width | Integer | Video's width in pixels |
| height | Integer | Video's height in pixels |
| video_length | Integer | Video's length in seconds |
| duration | Integer | Skipable duration in seconds |
| enabled | Boolean | Enable or disable |
