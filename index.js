const express = require('express')
const fs = require('fs')

/**
 * Server port from ENV.
 */
const port = process.env.PORT || 3000

/**
 * Return integer between 0 to given max value
 * @param {int} max 
 */
const getRandomIndex = (max) => {
    return Math.floor(Math.random() * Math.floor(max));
}

/**
 * Return randomed item from given array
 * @param {Array} arr 
 */
const getRandomItemInArray = (arr) => {
    const randomedIndex = getRandomIndex(arr.length)
    return arr[randomedIndex]
}

/**
 * Return ads list by given type.
 * @param {string} type 
 */
const getAdsList = (type) => {
    return new Promise((resolve, reject) => {
        fs.readFile('./ads.json', 'utf8', (err, contents) => {
            if (err) {
                reject(err)
            } else {
                adsList = JSON.parse(contents).filter(ads => ads.enabled)
                if (type) {
                    filteredAdsList = adsList.filter(ads => ads.type === type)
                    resolve(filteredAdsList)
                } else {
                    resolve(adsList)
                }
            }
        })
    })
}

/**
 * Return randomed ads by given type.
 * @param {string} type 
 */
const getRandomAds = async (type) => {
    const adsList = await getAdsList(type)
    const ads = getRandomItemInArray(adsList)
    return ads
}

/**
 * Create express application.
 */
const app = express()

/**
 * Route for get random ads
 */
app.get('/ads', async (req, res) => {
    const ads = await getRandomAds(req.query.type)
    res.json(ads)
})

/**
 * Start server.
 */
app.listen(port, () => {
    console.log(`App listening on port ${port}`)
})
